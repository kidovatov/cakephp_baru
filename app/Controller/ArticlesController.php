<?php
	App::uses('AppController', 'Controller');
	class ArticlesController extends AppController {
		
		// INI YANG DITAMBAHKAN
	
	public function index() {
	$this->set('articles', $this->Article->find('all'));	
	}

	public function view($id=null) {
	$this->Article->id=$id;
	$this->set('article', $this->Article->read(null, $id));	
	}
	
	public function add() {
		if($this->request->is('post')) {
			$this->Article->create();
			if($this->Article->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved'));
				$this->redirect(array('action'=>'index'));				
				} else {
				$this->Session->setFlash(__('The article couldnt be saved'));					
					}
			}		
		}
		
		public function edit($id=null) {
		$this->Article->id=$id;
			if($this->request->is('post') || $this->request->is('put')) {
				if($this->Article->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved'));
				$this->redirect(array('action'=>'index'));				
				} else {
				$this->Session->setFlash(__('The article couldnt be saved'));					
					}
		} else {
				$this->request->data = $this->Article->read(null,$id);				
				}
			}
			
			public function delete($id=null) {
		$this->Article->id=$id;
			if($this->Article->delete()) {
				$this->redirect(array('action'=>'index'));				
				}		
				$this->Session->setFlash(__('Article wasnt deleted'));
				$this->redirect(array('action'=>'index'));
		}
		
		}
?>