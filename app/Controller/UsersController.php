<?php

class UsersController extends AppController {
	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('add');
		}

	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
		}
		
	public function view ($id = null) {
		$this->User->id = $id;
		if(!$this->User->exists()) {
			throw new NotFoundException(__('Invalid Users'));
			}		
		$this->set('user', $this->User->read(null, $id));
		}		
		
/* Ini fungsi add() */
		public function add() {
			if($this->request->is('post')) {
				$this->User->create();
				if($this->User->save($this->request->data)) {
					$this->Session->setFlash(__('The user has been saved'))
					return $this->redirect(array('action'=>'index'));
					}
		$this->Session->setFlash(__('The user couldnt be saved'));					
					
					}			
			}
		
/* Ini merupakan fungsi EDIT */	
			public function edit($id=null) {
				$this->User->id = $id;
				if(!$this->User->exists()) {
					throw new NotFoundException(__('Invalid user'));					
					}
					
			if(!$this->User->exists()) {
					throw new NotFoundException(__('Invalid User'));
					}
					
			if($this->request->is('post') || $this->request->is('put')) {
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash(__('The user has been saved'));
					return $this->redirect(array('action'=>'index'));
					}
					$this->Session->setFlash(__('the user couldnt be saved');				
				} else {
					$this->request->data = $this->User->read(null, $id);
					unset($this->request->data['User']['password']);
		}
}

/* Ini fungsi DELETE */
	public function delete($id = null) {
		$this->request->onlyAllow('post');
		
		$this->User->id = $id;
		if(!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
			}
		if($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			return $this->redirect(array('action'=>'index'));			
			}			
			$this->Session->setFlash(__('User wasnt deleted'));
			return $this->redirect(array('action'=>'index'));
			}

/* Fungsi Before filter */
public function beforeFilter() {
	parent::beforeFilter();
	// memperbolehkan user untuk meregister dan logout
	$this->Auth->allow('add', 'logout');
	}	

/* Fungsi login */
public function login(){
	if($this->request->is('post')) {
		if($this->Auth->login()) {
			return $this->redirect($this->Auth->redirect());
			}	
			$this->Session->setFlash(__('Invalid username or password'));	
		}	
	}
/* Fungsi logout */
public function logout()	{
return $this->redirect($this->Auth->logout());	
	}		
			
			}
?>