<?php
	class EmployeesController extends AppController {
		public $helpers = array ('Html', 'Form');
		
		public function index() {
			$this->set('employees', $this->Employee->find('all'));			
		}
			
		function tambah() {
			if(!empty($this->data)) {
				$this->Employee->create();
				if($this->Employee->save($this->data)) {
		$this->Session->setFlash('Data berhasil ditambah');
		$this->redirect(array('action'=>'index'), null, true);
} else {
	$this->Session->setFlash('Data gagal ditambah, silahkan coba lagi');
}}}

function ubah($id=null) {
	if(!$id) {
	$this->Session->setFlash('Data tidak valid');
	$this->redirect(array('action'=>'index'), null, true);
}
	if(empty($this->data)) {
	$this->data = $this->Employee->findById($id);
} else {
	if($this->Employee->save($this->data)) {
	$this->Session->setFlash('Data berhasil diupdate');
	$this->redirect(array('action'=>'index'),null,true);
} else { 
	$this->Session->setFlash('Data gagal diupdate, silahkan coba lagi');
}}}

function hapus($id=null) {
	if(!$id) {
	$this->Session->setFlash('Data tidak valid');
	$this->redirect(array('action'=>'index'),null,true);
}
	if($this->Employee->delete($id)) {
	$this->Session->setFlash('Data dengan nama ' .$id. ' dihapus');
	$this->redirect(array('action'=>'index'),null,true);
}}}
?>
