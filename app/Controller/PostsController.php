<?php
	class PostsController extends AppController {
		var $name = 'Posts';
	
	public $helpers = array('Html', 'Form', 'Session');
	public $components = array('Session');

/* Fungsi untuk mengedit */
	public function edit($id=null) {
	if(!$id) {
			throw new NotFoundException(__('Invalid Post'));
			}
	$post=$this->Post->findById($id);
	if(!$post){
			throw new NotFoundException(__('Invalid Post'));		
		}
		
		if($this->request->is(array('post', 'put'))) {
			$this->Post->id=$id;
			if($this->Post->save($this->request->data)){
				$this->Session->setFlash(__('Your post has been updated'));
				return $this->redirect(array('action'=>'index'));
				}
				$this->Session->setFlash(__('Unable to update your post'));
				}
				
				if(!$this->request->data){
					$this->request->data=$post;
					}		
		}

/* Fungsi untuk menghapus */
	public function delete($id) {
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
			}
			
		if($this->Post->delete($id)){
			$this->Session->setFlash(__('The post with id: %s has been deleted', h($id)));
			return $this->redirect(array('action'=>'index'));
		}
	}	 	

	function hello_world() {
			}

	public function index() {
		$this->set('posts', $this->Post->find('all'));	
	}	

	public function view($id) {
		if(!$id) { throw new NotFoundException(__('Invalid post'));
		}
		
		$post = $this->Post->findById($id);
		if (!$post) { throw new NotFoundException(__('Invalid post'));
		}
		$this->set('post', $post);	
	}

/* Fungsi untuk menambahkan */
	public function add(){
	if($this->request->is('post')) {
		
		/* YANG DITAMBAHKAN */
	$this->request->data['Post']['user_id'] = $this->Auth->user('id');
	if($this->Post->save($this->request->data)) {
		$this->Session->setFlash(__('your post has been saved'));
		return $this->redirect(array('action'=>'index'));		
		}
		/* $this->Session->setFlash(__('Unable to add your post'));	*/	
		}
	}
}
?>