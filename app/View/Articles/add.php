<?php
	public function add() {
		if($this->request->is('post')) {
			$this->Article->create();
			if($this->Article->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved'));
				$this->redirect(array('action'=>'index'));				
				} else {
				$this->Session->setFlash(__('The article couldnt be saved'));					
					}
			}		
		}
?>