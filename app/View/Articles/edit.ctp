<div class = "articles form">
	<?php echo $this->Form->create('Article'); ?>
	<fieldset>
		<legend> <?php echo __('Edit Article'); ?> </legend>
		
		<?php
		echo $this->Form->input('id', array('type'=>'hidden'));		
		echo $this->Form->input('title');		
		echo $this->Form->input('summary');		
		echo $this->Form->input('content');
		?>
	</fieldset>
	
	<?php echo $this->Form->end(__('Submit')); ?>
</div>