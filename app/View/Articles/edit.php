<?php
	public function edit($id=null) {
		$this->Article->id=$id;
			if($this->request->is('post') || $this->request->is('put')) {
				if($this->Article->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved'));
				$this->redirect(array('action'=>'index'));				
				} else {
				$this->Session->setFlash(__('The article couldnt be saved'));					
					}
		} else {
				$this->request->data = $this->Article->read(null,$id);				
				}
			}
?>