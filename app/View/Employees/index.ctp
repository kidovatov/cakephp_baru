<?php
	echo $this->Html->link('Tambah Data', array('controller'=>'employees', 'action'=>'tambah')); 
?>

<table>
<tr>
<th> NIP </th>
<th> Nama </th>
<th> Golongan </th>
<th> Pangkat </th>
<th> Action </th>
</tr>

<?php foreach($employees as $employee); ?>

<tr>
<td><?php echo $employee['Employee']['nip']; ?> </td>
<td><?php echo $employee['Employee']['nama']; ?> </td>
<td><?php echo $employee['Employee']['golongan']; ?> </td>
<td><?php echo $employee['Employee']['pangkat']; ?> </td>
<td><?php echo $this->Html->link('Ubah', array('action' => 'ubah', $employee['Employee']['id'])); ?> |
<?php echo $this->Html->link('Hapus', array('action' => 'hapus', $employee['Employee']['id']),
	array('confirm'=>'Apakah Anda yakin akan menghapus data ini?')); ?> </td>
</tr>
<?php endforeach; ?>
</table>

