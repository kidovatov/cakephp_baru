<?php
echo $this->form->create('Employee');
?>

<fieldset>
<legend>Edit Data</legend>

<?php
echo $this->form->hidden('id');
echo $this->form->input('nip');
echo $this->form->input('nama');
echo $this->form->input('golongan');
echo $this->form->input('pangkat');
?>

</fieldset>
<?php echo $this->form->end('Update'); ?>
<?php echo $this->Html->link('Lihat Data', array('controller'=>'employees','action'=>'index')); ?>
