<?php
	class Article extends AppModel {
		public $name = 'Article';		
		
		public $validate= array(
			'title' 			=>array(
				'required'	=>array(
					'rule'	=>array('notEmpty'),
					'message'=>'Harus diisi',
					)
					),
			'description'	=>array(
				'required'	=>array(
					'rule'	=>array('notEmpty'),
					'message'=>'Harus diisi',)
			)			
			 );
		}
?>